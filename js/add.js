function createNewUser() {
    let userName = prompt("Введіть ваше імя")
    let lastUserName = prompt("Введіть вашу фамілію")
    const newUser = {
        firstName: userName,
        lastName: lastUserName,
        getLogin: function () {
            return (newUser.firstName.charAt(0) + newUser.lastName).toLowerCase();
        }
    }
    return newUser;
}
const user  = createNewUser();
console.log(user.getLogin());